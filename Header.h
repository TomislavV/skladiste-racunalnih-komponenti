#pragma once


#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct Product_Type1 {
	char name[30];
	char type[30];
	char soket[10];
	float price;
	int quantity;
	int id;
}PRODUCT_TYPE1;

typedef struct Product_Type2 {
	char name[30];
	char type[30];
	int  VRAM;//Ram 
	float price;
	int quantity;
	int id;
}PRODUCT_TYPE2;

typedef struct Product_Type3 {
	char name[30];
	char type[30];
	float price;
	int quantity;
	int id;
}PRODUCT_TYPE3;

//creating all bin files whic is needed for program.
void CreatingFiles();

//manus for program
void menu();
void EnterMenu();
void ShowMenu();
void SellMenu();

//enter the product in wealhouse 
void EnterTheProductType1(char[30]);//entering the name of the file for whic type you wont the product procesor or 
void EnterTheProductType2(char[30]);
void EnterTheProductType3(char[30]);

//geting products 
PRODUCT_TYPE1* GetingProductType1(PRODUCT_TYPE1*, int*, char*);//Pointer of product, pointer on number of product, pointer for name of file.
PRODUCT_TYPE2* GetingProductType2(PRODUCT_TYPE2*, int*, char*);
PRODUCT_TYPE3* GetingProductType3(PRODUCT_TYPE3*, int*, char*);

//printing products 
void PrintAllProductsProcesorsAndMotherboard(PRODUCT_TYPE1*, char*, int);//pointer for name of file that is need to open. 
void PrintAllProductsGraphicCardAndRAM(PRODUCT_TYPE2*, char*, int);
void PrintAllProductsCaseAndCooler(PRODUCT_TYPE3*, char*, int);

//BubleSort
PRODUCT_TYPE1* BubleSortProcesorsAndMotherboard(PRODUCT_TYPE1*, int*);
PRODUCT_TYPE2* BubleSortedGraphicCardAndRAM(PRODUCT_TYPE2*, int*);
PRODUCT_TYPE3* BubleSorteCaseAndCooler(PRODUCT_TYPE3*, int*);

//Selling items 
int SellProcesorsAndMotherboard(char*, int, char*, int*);
int SellGraphicCardAndRAM(char*, int, char*, int*);
int SellCaseAndCooler(char*, int, char*, int*);

