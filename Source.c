#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Header.h"

/*
	To do list:
		otvaranje i zatvaranje datoteke funkcije
		ubacivanje proizvoda u datoteku
		citanje i ispis po nekom kriteriju proizvoda
		Proizvodi:
			maticna ploca:
				-naziv
				-model
				-soket
				-cijena
			procesor:
				-naziv
				-model
				-soket
				-cijena
			graficka kartica:
				-naziv
				-model
				-Vram memorija
				-cijena
			Ram:
				-naziv
				-model
				-kolicinarama
				-cijena
			hladnjaci:
				-naziv
				-model
				-cijena
			kuciste:
				-naziv
				-model
				-cijena
		Tip1
		 -procesor i maticna ploca
			 naziv char[30]
			 model char[30]
			 soket char[10]
			 cijena float
		Tip2
		 -graficka kartica i ram
			naziv char [30]
			model char[30]
			Vram memorija int
			cijena float
		Tip3
		 -hladnjaci i kucista
			naziv char[30]
			model char [5]
			cijena 	float
*/
//glavni izbornik


void menu() {
	int select = -1;
	do {
		system("cls");
		printf("This is menu :\n\n");
		printf("1)Enter new product in warehaus.\n");
		printf("2)Show item.\n");
		printf("3)Sell item.\n");
		printf("4)Exit program.\n");
		//printf("Enther the value of line which you wont to execute.\n");
		do {
			scanf(" %d", &select);
			if (select < 1 || select>4) {
				printf("Enter the right value:\n");
			}
		} while (select < 1 || select>4);

		switch (select)
		{
		case 1:
			EnterMenu();
			break;

		case 2:
			ShowMenu();
			break;
		case 3:
			SellMenu();
			break;
		case 4:
			//exit
			return;
			break;
		}

	} while (select > 0);
}

void EnterMenu() {
	int select = -1;
	do {
		system("cls");
		printf("This is menu for entering new items.\n");
		printf("Select which item you want enter in warehouse.\n");
		printf("1)Procesor\n");
		printf("2)Graphic procesor\n");
		printf("3)RAM\n");
		printf("4)Cooler\n");
		printf("5)Case\n");
		printf("6)MotherBoard\n");
		printf("7)If you want exit\n");

		do {
			scanf(" %d", &select);
			if (select < 1 || select>7) {
				printf("Enter the right value:\n");
			}
		} while (select < 1 || select>7);

		switch (select)
		{
		case 1:
			//procesor
			EnterTheProductType1("Procesor");
			break;
		case 2:
			//grafika
			EnterTheProductType2("GraphicCard");
			break;
		case 3:
			//ram
			EnterTheProductType2("RAM");
			break;
		case 4:
			//hladnjak
			EnterTheProductType3("Cooler");
			break;
		case 5:
			//kuciste
			EnterTheProductType3("Case");   //
			break;
		case 6:
			//matherboard
			EnterTheProductType1("Motherboard"); //
			break;
		case 7:
			//exit 
			return;
			break;
		}
	} while (select > 0);
}

void ShowMenu() {
	int select = -1;
	int counter = 0;
	PRODUCT_TYPE1* Procesor = NULL;
	PRODUCT_TYPE1* Motherboard = NULL;
	PRODUCT_TYPE2* GraphicCard = NULL;
	PRODUCT_TYPE2* RAM = NULL;
	PRODUCT_TYPE3* Cooler = NULL;
	PRODUCT_TYPE3* Case = NULL;
	do {
		system("cls");
		printf("This is menu for Showing items.\n");
		printf("1)Processor\n");
		printf("2)Graphic Card\n");
		printf("3)RAM\n");
		printf("4)Cooler\n");
		printf("5)Case\n");
		printf("6)MotherBoard\n");
		printf("7)If you wont exit from program\n");

		do {
			scanf(" %d", &select);
			if (select < 1 || select>7) {
				printf("Enter the right value:\n");
			}
		} while (select < 1 || select>7);

		switch (select)
		{
		case 1:
			Procesor = GetingProductType1(Procesor, &counter, "Procesor.bin");
			if (Procesor == NULL) {
				printf("Fail.");
				exit(EXIT_FAILURE);
			}
			Procesor = BubleSortProcesorsAndMotherboard(Procesor, &counter);
			PrintAllProductsProcesorsAndMotherboard(Procesor, "Procesor.bin", counter);
			free(Procesor);
			counter = 0;
			break;
		case 2:
			GraphicCard = GetingProductType2(GraphicCard, &counter, "GraphicCard.bin");   //bez h
			if (GraphicCard == NULL) {
				printf("Fail.");
				exit(EXIT_FAILURE);
			}
			GraphicCard = BubleSortedGraphicCardAndRAM(GraphicCard, &counter);
			PrintAllProductsGraphicCardAndRAM(GraphicCard, "GraphicCard.bin", counter);
			free(GraphicCard);
			counter = 0;
			break;
		case 3:
			RAM = GetingProductType2(RAM, &counter, "RAM.bin");
			if (RAM == NULL) {
				printf("Fail.");
				exit(EXIT_FAILURE);
			}
			RAM = BubleSortedGraphicCardAndRAM(RAM, &counter);
			PrintAllProductsGraphicCardAndRAM(RAM, "RAM.bin", counter);
			free(RAM);
			counter = 0;
			break;
		case 4:
			Cooler = GetingProductType3(Cooler, &counter, "Cooler.bin");
			if (Cooler == NULL) {
				printf("Fail.");
				exit(EXIT_FAILURE);
			}
			Cooler = BubleSorteCaseAndCooler(Cooler, &counter);
			PrintAllProductsCaseAndCooler(Cooler, "Cooler.bin", counter);
			free(Cooler);
			counter = 0;
			break;
		case 5:
			Case = GetingProductType3(Case, &counter, "Case.bin");  //
			if (Case == NULL) {
				printf("Fail.");
				exit(EXIT_FAILURE);
			}
			Case = BubleSorteCaseAndCooler(Case, &counter);
			PrintAllProductsCaseAndCooler(Case, "Case.bin", counter);
			free(Case);
			counter = 0;
			break;
		case 6:
			Motherboard = GetingProductType1(Motherboard, &counter, "Motherboard.bin");
			if (Motherboard == NULL) {
				printf("Fail.");
				exit(EXIT_FAILURE);
			}
			Motherboard = BubleSortProcesorsAndMotherboard(Motherboard, &counter);
			PrintAllProductsProcesorsAndMotherboard(Motherboard, "Motherboard.bin", counter);
			free(Motherboard);
			counter = 0;
			break;
		case 7:
			//exit 
			return;
			break;
		}
	} while (select > 0);
}

void SellMenu() {
	int select = -1;
	char nameOfSearchItem[30];
	int idOfSearchItem = -1;
	int check = -1;
	int counter = 0;
	do {
		system("cls");
		printf("This is menu for Sell items.\n");
		printf("1)Procesor\n");
		printf("2)Graphic procesor\n");
		printf("3)RAM\n");
		printf("4)Coler\n");
		printf("5)Case\n");
		printf("6)Motherboard\n");
		printf("7)If you want exit from program\n");
		do {
			scanf(" %d", &select);
			if (select < 1 || select>7) {
				printf("Enter the right value:\n");
			}
		} while (select < 1 || select>7);
		if (select < 7) {
			printf("Enther the name of product you want sell.\n");
			scanf(" %29s", &nameOfSearchItem);
			printf("Enter the id of that item.\n");
			scanf(" %d", &idOfSearchItem);
		}
		counter = 0;
		switch (select)
		{
		case 1:
			/*
				removing quantity of procesor from warehouse if the item exists.
			*/
			check = SellProcesorsAndMotherboard(nameOfSearchItem, idOfSearchItem, "Procesor.bin", &counter);
			if (check > 0) {
				printf("The Purchase is succesfull.\n");
			}
			else {
				printf("The Purchase is not succesfull.Try again.\n");
			}
			check = -1;
			break;
		case 2:
			//grafika
			check = SellGraphicCardAndRAM(nameOfSearchItem, idOfSearchItem, "GraphicCard.bin", &counter);
			if (check > 0) {
				printf("The Purchase is succesfull.\n");
			}
			else {
				printf("The Purchase is not succesfull.Try again.\n");
			}
			check = -1;
			break;
		case 3:
			//ram
			check = SellGraphicCardAndRAM(nameOfSearchItem, idOfSearchItem, "RAM.bin", &counter);
			if (check > 0) {
				printf("The Purchase is succesfull.\n");
			}
			else {
				printf("The Purchase is not succesfull.Try again.\n");
			}
			check = -1;
			break;
			break;
		case 4:
			//hladnjak
			check = SellCaseAndCooler(nameOfSearchItem, idOfSearchItem, "Cooler.bin", &counter);
			if (check > 0) {
				printf("The Purchase is succesfull.\n");
			}
			else {
				printf("The Purchase is not succesfull.Try again.\n");
			}
			check = -1;
			break;
		case 5:
			//kuciste
			check = SellCaseAndCooler(nameOfSearchItem, idOfSearchItem, "Case.bin", &counter);
			if (check > 0) {
				printf("The Purchase is succesfull.\n");
			}
			else {
				printf("The Purchase is not succesfull.Try again.\n");
			}
			check = -1;
			break;
		case 6:
			check = SellProcesorsAndMotherboard(nameOfSearchItem, idOfSearchItem, "Motherboard.bin", &counter);
			if (check > 0) {
				printf("The Purchase is succesfull.\n");
			}
			else {
				printf("The Purchase is not succesfull.Try again.\n");
			}
			check = -1;
			break;
		case 7:
			//exit 
			return;
			break;
		}
		_getch();
	} while (select > 0);
}

void CreatingFiles() {
	FILE* Procesor, * Motherboard, * Case;
	FILE* Cooler, * GraphicCard, * RAM;
	Procesor = fopen("Procesor.bin", "rb+");
	if (Procesor == NULL) {
		Procesor = fopen("Procesor.bin", "wb+");
		fclose(Procesor);
	}
	Motherboard = fopen("Motherboard.bin", "rb+");
	if (Motherboard == NULL) {
		Motherboard = fopen("Motherboard.bin", "wb+");
		fclose(Motherboard);
	}
	Case = fopen("Case.bin", "rb+");
	if (Case == NULL) {
		Case = fopen("Case.bin", "wb+");
		fclose(Case);
	}
	Cooler = fopen("Cooler.bin", "rb+");
	if (Cooler == NULL) {
		Cooler = fopen("Cooler.bin", "wb+");
		fclose(Cooler);
	}
	GraphicCard = fopen("GraphicCard.bin", "rb+");
	if (GraphicCard == NULL) {
		GraphicCard = fopen("GraphicCard.bin", "wb+");
		fclose(GraphicCard);
	}
	RAM = fopen("RAM.bin", "rb+");
	if (RAM == NULL) {
		RAM = fopen("RAM.bin", "wb+");
		fclose(RAM);
	}
}

void EnterTheProductType1(char FileName[30]) {
	//type 1 product 
	int temp = -1;
	int* quantity;
	quantity = &temp;
	FILE* file = NULL;
	if ((strcmp(FileName, "Procesor") == 0)) {
		//enter the procesors
		PRODUCT_TYPE1* Procesors;
		file = fopen("Procesor.bin", "rb+");

		rewind(file);
		fread(quantity, sizeof(int), 1, file);
		if ((*quantity) < 0) {
			*quantity = 1;
		}
		else {
			(*quantity)++;
		}
		rewind(file);
		fwrite(quantity, sizeof(int), 1, file);
		Procesors = (PRODUCT_TYPE1*)calloc(1, sizeof(PRODUCT_TYPE1));
		if (Procesors == NULL) {
			perror("No more space for new product.\n");
			exit(EXIT_FAILURE);
		}
		else {
			printf("Enter the name of processor:\n");
			scanf(" %29s", &Procesors->name);
			printf("Enter the type of procesor:\n");
			scanf(" %29s", &Procesors->type);
			printf("Enter the socket of procesor:\n");
			scanf(" %9s", &Procesors->soket);
			printf("Enter the price of procesor:\n");
			scanf(" %f", &Procesors->price);
			printf("Enter the quantity of procesor:\n");
			scanf(" %d", &Procesors->quantity);
			Procesors->id = *(quantity);
			fseek(file, 0, SEEK_END);
			fwrite(Procesors, sizeof(PRODUCT_TYPE1), 1, file);
		}
		fclose(file);
		free(Procesors);
	}
	else {
		PRODUCT_TYPE1* Motherboard;
		file = fopen("Motherboard.bin", "rb+");
		rewind(file);
		fread(quantity, sizeof(int), 1, file);
		if ((*quantity) < 0) {
			*quantity = 1;
		}
		else {
			(*quantity)++;
		}
		rewind(file);
		fwrite(quantity, sizeof(int), 1, file);

		Motherboard = (PRODUCT_TYPE1*)calloc(1, sizeof(PRODUCT_TYPE1));
		if (Motherboard == NULL) {
			perror("No more space for new product.\n");
			exit(EXIT_FAILURE);
		}
		else {
			printf("Enter the name of Motherboard:\n");
			scanf(" %29s", &Motherboard->name);
			printf("Enter the type of Motherboard:\n");
			scanf(" %29s", &Motherboard->type);
			printf("Enter the socket of Motherboard:\n");
			scanf(" %9s", &Motherboard->soket);
			printf("Enter the price of Motherboard:\n");
			scanf(" %f", &Motherboard->price);
			printf("Enter the quantity of Motherboard:\n");
			scanf(" %d", &Motherboard->quantity);
			Motherboard->id = *(quantity);
			fseek(file, 0, SEEK_END);
			fwrite(Motherboard, sizeof(PRODUCT_TYPE1), 1, file);
		}
		fclose(file);
		free(Motherboard);
	}
	return;
}

void EnterTheProductType2(char FileName[30]) {
	int temp = -1;
	int* quantity;
	quantity = &temp;
	FILE* file = NULL;
	if ((strcmp(FileName, "GraphicCard")) == 0) {
		//enter the GraphichCard
		PRODUCT_TYPE2* GraphicCard = NULL;
		file = fopen("GraphicCard.bin", "rb+");
		if (file == NULL) {
			perror("Fali.");
			exit(EXIT_FAILURE);
		}


		GraphicCard = (PRODUCT_TYPE2*)calloc(1, sizeof(PRODUCT_TYPE2));
		if (GraphicCard == NULL) {
			perror("No more space for new product.\n");
			exit(EXIT_FAILURE);
		}
		else {
			rewind(file);
			fread(quantity, sizeof(int), 1, file);
			(*quantity)++;
			rewind(file);
			fwrite(quantity, sizeof(int), 1, file);

			printf("Enter the name of GraphicCard:\n");
			scanf(" %29s", &GraphicCard->name);
			printf("Enther the type of GraphicCard:\n");
			scanf(" %29s", &GraphicCard->type);
			printf("Enther the VRAM of GraphicCard:\n");
			scanf(" %d", &GraphicCard->VRAM);
			printf("Enther the price of GraphicCard:\n");
			scanf(" %f", &GraphicCard->price);
			printf("Enther the quantity of GraphichCard:\n");
			scanf(" %d", &GraphicCard->quantity);
			GraphicCard->id = *(quantity);
			fseek(file, 0, SEEK_END);
			fwrite(GraphicCard, sizeof(PRODUCT_TYPE2), 1, file);
			fclose(file);
			free(GraphicCard);

		}

	}
	if ((strcmp(FileName, "RAM")) == 0) {
		//enter the procesor
		PRODUCT_TYPE2* RAM;
		file = fopen("RAM.bin", "rb+");

		rewind(file);
		fread(quantity, sizeof(int), 1, file);
		if ((*quantity) < 0) {
			*quantity = 1;
		}
		else {
			(*quantity)++;
		}
		rewind(file);
		fwrite(quantity, sizeof(int), 1, file);
		RAM = (PRODUCT_TYPE2*)calloc(1, sizeof(PRODUCT_TYPE2));
		if (RAM == NULL) {
			perror("No more space for new product.\n");
			exit(EXIT_FAILURE);
		}
		else {
			printf("Enter the name of RAM:\n");
			scanf(" %29s", &RAM->name);
			printf("Enther the type of RAM:\n");
			scanf(" %29s", &RAM->type);
			printf("Enther the capacity of RAM:\n");
			scanf(" %d", &RAM->VRAM);
			printf("Enther the price of RAM:\n");
			scanf(" %f", &RAM->price);
			printf("Enther the quantity of RAM:\n");
			scanf(" %d", &RAM->quantity);
			RAM->id = *(quantity);
			fseek(file, 0, SEEK_END);
			fwrite(RAM, sizeof(PRODUCT_TYPE2), 1, file);
			fclose(file);
			free(RAM);

		}
	}
	return;
}

void EnterTheProductType3(char FileName[30]) {
	int temp = -1;
	int* quantity;
	quantity = &temp;
	FILE* file = NULL;
	if ((strcmp(FileName, "Case")) == 0) {
		//enter the GraphichCard
		PRODUCT_TYPE3* Case = NULL;
		file = fopen("Case.bin", "rb+");
		if (file == NULL) {
			perror("Fali.");
			exit(EXIT_FAILURE);
		}

		Case = (PRODUCT_TYPE3*)calloc(1, sizeof(PRODUCT_TYPE3));
		if (Case == NULL) {
			perror("No more space for new product.\n");
			exit(EXIT_FAILURE);
		}
		else {
			rewind(file);
			fread(quantity, sizeof(int), 1, file);
			(*quantity)++;
			rewind(file);
			fwrite(quantity, sizeof(int), 1, file);

			printf("Enter the name of Case:\n");
			scanf(" %29s", &Case->name);
			printf("Enther the type of Case:\n");
			scanf(" %29s", &Case->type);
			printf("Enther the price of Case:\n");
			scanf(" %f", &Case->price);
			printf("Enther the quantity of Case:\n");
			scanf(" %d", &Case->quantity);
			Case->id = *(quantity);
			fseek(file, 0, SEEK_END);
			fwrite(Case, sizeof(PRODUCT_TYPE3), 1, file);
			fclose(file);
			free(Case);
		}
	}
	if ((strcmp(FileName, "Cooler")) == 0) {
		PRODUCT_TYPE3* Cooler = NULL;
		file = fopen("Cooler.bin", "rb+");
		if (file == NULL) {
			perror("Fali.");
			exit(EXIT_FAILURE);
		}

		Cooler = (PRODUCT_TYPE3*)calloc(1, sizeof(PRODUCT_TYPE3));
		if (Cooler == NULL) {
			perror("No more space for new product.\n");
			exit(EXIT_FAILURE);
		}
		else {
			rewind(file);
			fread(quantity, sizeof(int), 1, file);
			(*quantity)++;
			rewind(file);
			fwrite(quantity, sizeof(int), 1, file);

			printf("Enter the name of Cooler:\n");
			scanf(" %29s", &Cooler->name);
			printf("Enther the type of Cooler:\n");
			scanf(" %29s", &Cooler->type);
			printf("Enther the price of Cooler:\n");
			scanf(" %f", &Cooler->price);
			printf("Enther the quantity of Cooler:\n");
			scanf(" %d", &Cooler->quantity);
			Cooler->id = *(quantity);
			fseek(file, 0, SEEK_END);
			fwrite(Cooler, sizeof(PRODUCT_TYPE3), 1, file);
			fclose(file);
			free(Cooler);
		}
	}
	return;
}  ////

//geting from files products
PRODUCT_TYPE1* GetingProductType1(PRODUCT_TYPE1* Product, int* counter, char* fileName) {//dohvacanje procesora i maticne ploce
	FILE* file = NULL;
	file = fopen(fileName, "rb+");
	if (file == NULL) {
		printf("It is not possible to open file.");
		exit(EXIT_FAILURE);
	}
	else {
		rewind(file);
		fread(counter, sizeof(int), 1, file);
		Product = (PRODUCT_TYPE1*)calloc(*counter, sizeof(PRODUCT_TYPE1));
		if (Product == NULL) {
			printf("That is too much products for memory.");
			exit(EXIT_FAILURE);
		}
		else {
			fread(Product, sizeof(PRODUCT_TYPE1), *counter, file);
			fclose(file);
			return Product;
		}
	}
}

PRODUCT_TYPE2* GetingProductType2(PRODUCT_TYPE2* Product, int* counter, char* fileName) {//dohvacanje graficka kartica i ram 
	FILE* file = NULL;
	file = fopen(fileName, "rb+");
	if (file == NULL) {
		printf("It is not possible to open file.");
		exit(EXIT_FAILURE);
	}
	else {
		rewind(file);
		fread(counter, sizeof(int), 1, file);
		Product = (PRODUCT_TYPE2*)calloc(*counter, sizeof(PRODUCT_TYPE2));
		if (Product == NULL) {
			printf("That is too much products for memory.");
			exit(EXIT_FAILURE);
		}
		else {
			fread(Product, sizeof(PRODUCT_TYPE2), *counter, file);
			fclose(file);
			return Product;
		}
	}
}

PRODUCT_TYPE3* GetingProductType3(PRODUCT_TYPE3* Product, int* counter, char* fileName) {//dohvacanje -hladnjaci i kucista 
	FILE* file = NULL;
	file = fopen(fileName, "rb+");
	if (file == NULL) {
		printf("It is not possible to open file.");
		exit(EXIT_FAILURE);
	}
	else {
		rewind(file);
		fread(counter, sizeof(int), 1, file);

		Product = (PRODUCT_TYPE3*)calloc(*counter, sizeof(PRODUCT_TYPE3));
		if (Product == NULL) {
			printf("That is too mach products for memory.");
			exit(EXIT_FAILURE);
		}
		else {
			fread(Product, sizeof(PRODUCT_TYPE3), *counter, file);
			fclose(file);
			return Product;
		}
	}
}

//Printign on screen all items
//If no item in the file program print "Ther is no item for show."
void PrintAllProductsProcesorsAndMotherboard(PRODUCT_TYPE1* Product, char* fileName, int counter) {
	FILE* file = NULL;
	file = fopen(fileName, "rb+");
	if (file == NULL) {
		printf("It is not possible to open file.");
		exit(EXIT_FAILURE);
	}
	else {
		if (strcmp("Procesor.bin", fileName) == 0) {
			if (counter != 0) {
				for (int i = 0; i < counter; i++) {
					printf("\nName of procesor:%s\nSoket of procesor:%s\nPrice of procesor:%f\nQuantity:%d\nId of product:%d\n",
						(Product + i)->name,
						(Product + i)->soket,
						(Product + i)->price,
						(Product + i)->quantity,
						(Product + i)->id);
				}
			}
			else {
				printf("There is no item.");
			}
		}
		else {
			if (counter != 0) {
				for (int i = 0; i < counter; i++) {
					printf("\nName of MotherBoard:%s\nSoket of MotherBoard:%s\nPrice of MotherBoard:%f\nQuantity:%d\nId of product:%d\n",
						(Product + i)->name,
						(Product + i)->soket,
						(Product + i)->price,
						(Product + i)->quantity,
						(Product + i)->id);
				}
			}
			else {
				printf("Theer is no item.");
			}
		}
	}
	fclose(file);
	_getch();
}

void PrintAllProductsGraphicCardAndRAM(PRODUCT_TYPE2* Product, char* fileName, int counter) {
	FILE* file = NULL;
	file = fopen(fileName, "rb+");
	if (file == NULL) {
		printf("It is not possible to open file.");
		exit(EXIT_FAILURE);
	}
	else {
		if (strcmp("GraphicCard.bin", fileName) == 0) {
			if (counter != 0) {
				for (int i = 0; i < counter; i++) {

					printf("\nName of GraphicCard:%s\nType of GraphicCard:%s\n VRAM:%d\nPrice of GraphicCard:%f\nQuantity:%d\nId of product:%d\n",
						(Product + i)->name,
						(Product + i)->type,
						(Product + i)->VRAM,
						(Product + i)->price,
						(Product + i)->quantity,
						(Product + i)->id);
				}
			}
			else {
				printf("There is no item.");
			}
		}
		else {
			if (counter != 0) {
				for (int i = 0; i < counter; i++) {
					printf("\nName of RAM:%s\nType of RAM:%s\n RAM GB:%d\nPrice of RAM:%f\nQuantity:%d\nId of product:%d\n",
						(Product + i)->name,
						(Product + i)->type,
						(Product + i)->VRAM,
						(Product + i)->price,
						(Product + i)->quantity,
						(Product + i)->id);
				}
			}
			else {
				printf("There is no item.");
			}
		}
	}
	fclose(file);
	_getch();
}

void PrintAllProductsCaseAndCooler(PRODUCT_TYPE3* Product, char* fileName, int counter) {
	FILE* file = NULL;
	file = fopen(fileName, "rb+");
	if (file == NULL) {
		printf("It is not possible to open file.");
		exit(EXIT_FAILURE);
	}
	else {
		if (strcmp("Case.bin", fileName) == 0) {
			if (counter != 0) {
				for (int i = 0; i < counter; i++) {
					printf("\nName of Case:%s\nType of Case:%s\nPrice of Case:%f\nQuantity:%d\nId of product:%d\n",
						(Product + i)->name,
						(Product + i)->type,
						(Product + i)->price,
						(Product + i)->quantity,
						(Product + i)->id);
				}
			}
			else {
				printf("There is no item.");
			}

		}
		else {
			if (counter != 0) {
				for (int i = 0; i < counter; i++) {
					printf("\nName of Cooler:%s\nType of Cooler:%s\nPrice of Cooler:%f\nQuantity:%d\nId of product:%d\n",
						(Product + i)->name,
						(Product + i)->type,
						(Product + i)->price,
						(Product + i)->quantity,
						(Product + i)->id);
				}
			}
			else {
				printf("There is no item.");
			}
		}
	}
	fclose(file);
	_getch();
}

//buble sort metods for all prodcts
PRODUCT_TYPE1* BubleSortProcesorsAndMotherboard(PRODUCT_TYPE1* Product, int* counter) {
	PRODUCT_TYPE1 Swap;

	for (int i = 0; i < (*counter) - 1; i++)
	{
		for (int j = 0; j < (*counter) - 1 - i; j++)
		{
			if ((Product + j + 1)->price < (Product + j)->price) {
				//save in swap from (Product + j + 1)
				strcpy(Swap.name, (Product + j + 1)->name);
				strcpy(Swap.type, (Product + j + 1)->type);
				strcpy(Swap.soket, (Product + j + 1)->soket);
				Swap.price = (Product + j + 1)->price;
				Swap.quantity = (Product + j + 1)->quantity;
				Swap.id = (Product + j + 1)->id;

				//save in  (Product + j + 1) from  (Product + j)
				strcpy((Product + j + 1)->name, (Product + j)->name);
				strcpy((Product + j + 1)->type, (Product + j)->type);
				strcpy((Product + j + 1)->soket, (Product + j)->soket);
				(Product + j + 1)->price = (Product + j)->price;
				(Product + j + 1)->quantity = (Product + j)->quantity;
				(Product + j + 1)->id = (Product + j)->id;

				//save in (Product + j) from swap
				strcpy((Product + j)->name, Swap.name);
				strcpy((Product + j)->type, Swap.type);
				strcpy((Product + j)->soket, Swap.soket);
				(Product + j)->price = Swap.price;
				(Product + j)->quantity = Swap.quantity;
				(Product + j)->id = Swap.id;
			}
		}
	}
	return Product;
}

PRODUCT_TYPE2* BubleSortedGraphicCardAndRAM(PRODUCT_TYPE2* Product, int* counter) {
	PRODUCT_TYPE2 Swap;

	for (int i = 0; i < (*counter) - 1; i++)
	{
		for (int j = 0; j < (*counter) - 1 - i; j++)
		{
			if ((Product + j + 1)->price < (Product + j)->price) {
				//save in swap from (Product + j + 1)
				strcpy(Swap.name, (Product + j + 1)->name);
				strcpy(Swap.type, (Product + j + 1)->type);
				Swap.VRAM = (Product + j + 1)->VRAM;
				Swap.price = (Product + j + 1)->price;
				Swap.quantity = (Product + j + 1)->quantity;
				Swap.id = (Product + j + 1)->id;

				//save in  (Product + j + 1) from  (Product + j)
				strcpy((Product + j + 1)->name, (Product + j)->name);
				strcpy((Product + j + 1)->type, (Product + j)->type);
				(Product + j + 1)->VRAM = (Product + j)->VRAM;
				(Product + j + 1)->price = (Product + j)->price;
				(Product + j + 1)->quantity = (Product + j)->quantity;
				(Product + j + 1)->id = (Product + j)->id;

				//save in (Product + j) from swap
				strcpy((Product + j)->name, Swap.name);
				strcpy((Product + j)->type, Swap.type);
				(Product + j)->VRAM = Swap.VRAM;
				(Product + j)->price = Swap.price;
				(Product + j)->quantity = Swap.quantity;
				(Product + j)->id = Swap.id;
			}
		}
	}
	return Product;
}

PRODUCT_TYPE3* BubleSorteCaseAndCooler(PRODUCT_TYPE3* Product, int* counter) {
	PRODUCT_TYPE3 Swap;

	for (int i = 0; i < (*counter) - 1; i++)
	{
		for (int j = 0; j < (*counter) - 1 - i; j++)
		{
			if ((Product + j + 1)->price < (Product + j)->price) {
				//save in swap from (Product + j + 1)
				strcpy(Swap.name, (Product + j + 1)->name);
				strcpy(Swap.type, (Product + j + 1)->type);
				Swap.price = (Product + j + 1)->price;
				Swap.quantity = (Product + j + 1)->quantity;
				Swap.id = (Product + j + 1)->id;

				//save in  (Product + j + 1) from  (Product + j)
				strcpy((Product + j + 1)->name, (Product + j)->name);
				strcpy((Product + j + 1)->type, (Product + j)->type);
				(Product + j + 1)->price = (Product + j)->price;
				(Product + j + 1)->quantity = (Product + j)->quantity;
				(Product + j + 1)->id = (Product + j)->id;

				//save in (Product + j) from swap
				strcpy((Product + j)->name, Swap.name);
				strcpy((Product + j)->type, Swap.type);
				(Product + j)->price = Swap.price;
				(Product + j)->quantity = Swap.quantity;
				(Product + j)->id = Swap.id;
			}
		}
	}
	return Product;
}

//Selling products 
int SellProcesorsAndMotherboard(char* nameOfSearchItem, int idOfSearchItem, char* fileName, int* counter) {
	int sellQuantity = 0;
	FILE* file = NULL;
	int temp = *(counter);
	int flag = 0;
	if ((strcmp("Procesor.bin", fileName)) == 0) {
		PRODUCT_TYPE1* Procesor = NULL;
		Procesor = GetingProductType1(Procesor, counter, "Procesor.bin");
		if (Procesor == NULL) {
			printf("Fail.");
			exit(EXIT_FAILURE);
		}
		for (int i = 0; i < *(counter); i++) {
			if ((strcmp((Procesor + i)->name, nameOfSearchItem) == 0) && (Procesor + i)->id == idOfSearchItem) {
				flag = 1;
				printf("Item is found.\n");
				printf("\nName of procesor:%s\nSoket of procesor:%s\nPrice of procesor:%f\nQuantity:%d\nId of product:%d\n",
					(Procesor + i)->name,
					(Procesor + i)->soket,
					(Procesor + i)->price,
					(Procesor + i)->quantity,
					(Procesor + i)->id);
				printf("How much processor you wont sell:\n");
				scanf(" %d", &sellQuantity);
				if (sellQuantity > (Procesor + i)->quantity) {
					printf("That purches for this processor is not possible\n");
					free(Procesor);
					return -1;
				}
				else {
					(Procesor + i)->quantity = (Procesor + i)->quantity - sellQuantity;
					printf("New quantity is %d\n", (Procesor + i)->quantity);
					break;
				}
			}
		}
		if (flag == 0)
		{
			free(Procesor);
			return -1;
		}
		//entering new value of quantity in the file 
		remove("Procesor.bin");
		file = fopen("Procesor.bin", "wb");
		if (file == NULL) {
			perror("Otvaranje dat:");
			exit(EXIT_FAILURE);
		}
		else {
			rewind(file);
			fwrite(counter, sizeof(int), 1, file);
			fseek(file, 4, SEEK_SET);
			for (int i = 0; i < (*counter); i++) {
				fwrite((Procesor + i), sizeof(PRODUCT_TYPE1), 1, file);
			}
		}
		fclose(file);
		free(Procesor);
		return 1;
	}
	else {
		PRODUCT_TYPE1* Motherboard = NULL;
		Motherboard = GetingProductType1(Motherboard, counter, "Motherboard.bin");
		if (Motherboard == NULL) {
			printf("Fail.");
			exit(EXIT_FAILURE);
		}
		for (int i = 0; i < *(counter); i++) {
			if ((strcmp((Motherboard + i)->name, nameOfSearchItem) == 0) && (Motherboard + i)->id == idOfSearchItem) {
				flag = 1;
				printf("Item is found.\n");
				printf("\nName of Motherboard:%s\nSoket of Motherboard:%s\nPrice of Motherboard:%f\nQuantity:%d\nId of product:%d\n",
					(Motherboard + i)->name,
					(Motherboard + i)->soket,
					(Motherboard + i)->price,
					(Motherboard + i)->quantity,
					(Motherboard + i)->id);
				printf("How much Motherboard you wont sell:\n");
				scanf(" %d", &sellQuantity);
				if (sellQuantity > (Motherboard + i)->quantity) {
					printf("That purches for this Motherboard is not possible\n");
					free(Motherboard);
					return -1;
				}
				else {
					(Motherboard + i)->quantity = (Motherboard + i)->quantity - sellQuantity;
					printf("New quantity is %d\n", (Motherboard + i)->quantity);
					break;
				}
			}
		}
		if (flag == 0)
		{
			free(Motherboard);
			return -1;
		}
		//entering new value of quantity in the file 
		remove("Motherboard.bin");
		file = fopen("Motherboard.bin", "wb");
		if (file == NULL) {
			perror("Otvaranje dat:");
			exit(EXIT_FAILURE);
		}
		else {
			rewind(file);
			fwrite(counter, sizeof(int), 1, file);
			fseek(file, 4, SEEK_SET);
			for (int i = 0; i < (*counter); i++) {
				fwrite((Motherboard + i), sizeof(PRODUCT_TYPE1), 1, file);
			}
		}
		fclose(file);
		free(Motherboard);
		return 1;
	}
	return -1;
}

int SellGraphicCardAndRAM(char* nameOfSearchItem, int idOfSearchItem, char* fileName, int* counter) {
	int sellQuantity = 0;
	FILE* file = NULL;
	int temp = *(counter);
	int flag = 0;
	if ((strcmp("GraphicCard.bin", fileName)) == 0) {
		PRODUCT_TYPE2* GraphicCard = NULL;
		GraphicCard = GetingProductType2(GraphicCard, counter, "GraphicCard.bin");
		if (GraphicCard == NULL) {
			printf("Fail.");
			exit(EXIT_FAILURE);
		}
		for (int i = 0; i < *(counter); i++) {
			if ((strcmp((GraphicCard + i)->name, nameOfSearchItem) == 0) && (GraphicCard + i)->id == idOfSearchItem) {
				flag = 1;
				printf("\nItem is found.\n");
				printf("Name of GraphicCard:%s\nType of GraphicCard:%s\n VRAM:%d\nPrice of GraphicCard:%f\nQuantity:%d\nId of product:%d\n",
					(GraphicCard + i)->name,
					(GraphicCard + i)->type,
					(GraphicCard + i)->VRAM,
					(GraphicCard + i)->price,
					(GraphicCard + i)->quantity,
					(GraphicCard + i)->id);
				printf("How much GraphicCard you wont sell:\n");
				scanf(" %d", &sellQuantity);
				if (sellQuantity > (GraphicCard + i)->quantity) {
					printf("That purchase for this GraphichCard is not possible\n");
					free(GraphicCard);
					return -1;
				}
				else {
					(GraphicCard + i)->quantity = (GraphicCard + i)->quantity - sellQuantity;
					printf("New quantity is %d\n", (GraphicCard + i)->quantity);
					break;
				}
			}
		}
		if (flag == 0) {
			free(GraphicCard);
			return -1;
		}
		//entering new value of quantity in the file 
		remove("GraphicCard.bin");
		file = fopen("GraphicCard.bin", "wb");
		if (file == NULL) {
			perror("Opening dat:");
			exit(EXIT_FAILURE);
		}
		else {
			rewind(file);
			fwrite(counter, sizeof(int), 1, file);
			fseek(file, 4, SEEK_SET);
			for (int i = 0; i < (*counter); i++) {
				fwrite((GraphicCard + i), sizeof(PRODUCT_TYPE2), 1, file);
			}
		}
		fclose(file);
		free(GraphicCard);
		return 1;
	}
	else {
		PRODUCT_TYPE2* RAM = NULL;
		RAM = GetingProductType2(RAM, counter, "RAM.bin");
		if (RAM == NULL) {
			printf("Fail.");
			exit(EXIT_FAILURE);
		}
		for (int i = 0; i < *(counter); i++) {
			if ((strcmp((RAM + i)->name, nameOfSearchItem) == 0) && (RAM + i)->id == idOfSearchItem) {
				flag = 1;
				printf("\nItem is found.\n");
				printf("Name of RAM:%s\nType of RAM:%s\n Gb of RAM memory:%d\nPrice of RAM:%f\nQuantity:%d\nId of product:%d\n",
					(RAM + i)->name,
					(RAM + i)->type,
					(RAM + i)->VRAM,
					(RAM + i)->price,
					(RAM + i)->quantity,
					(RAM + i)->id);
				printf("How much GraphicCard you wont sell:\n");
				scanf(" %d", &sellQuantity);
				if (sellQuantity > (RAM + i)->quantity) {
					printf("That purches for this GraphicCard is not possible\n");
					free(RAM);
					return -1;
				}
				else {
					(RAM + i)->quantity = (RAM + i)->quantity - sellQuantity;
					printf("New quantity is %d\n", (RAM + i)->quantity);
					break;
				}
			}
		}
		if (flag == 0) {
			free(RAM);
			return -1;
		}
		//entering new value of quantity in the file 
		remove("RAM.bin");
		file = fopen("RAM.bin", "wb");
		if (file == NULL) {
			perror("Opening dat:");
			exit(EXIT_FAILURE);
		}
		else {
			rewind(file);
			fwrite(counter, sizeof(int), 1, file);
			fseek(file, 4, SEEK_SET);
			for (int i = 0; i < (*counter); i++) {
				fwrite((RAM + i), sizeof(PRODUCT_TYPE2), 1, file);
			}
		}
		fclose(file);
		free(RAM);
		return 1;
	}
	return -1;
}

int SellCaseAndCooler(char* nameOfSearchItem, int idOfSearchItem, char* fileName, int* counter) {
	int sellQuantity = 0;
	FILE* file = NULL;
	int temp = *(counter);
	int flag = 0;
	if ((strcmp("Case.bin", fileName)) == 0) {
		PRODUCT_TYPE3* Case = NULL;
		Case = GetingProductType3(Case, counter, "Case.bin");
		if (Case == NULL) {
			printf("Fail.");
			exit(EXIT_FAILURE);
		}
		for (int i = 0; i < *(counter); i++) {
			if ((strcmp((Case + i)->name, nameOfSearchItem) == 0) && (Case + i)->id == idOfSearchItem) {
				flag = 1;
				printf("\nItem is found.\n");
				printf("\nName of Case:%s\nType of Case:%s\nPrice of Chase:%f\nQuantity:%d\nId of product:%d\n",
					(Case + i)->name,
					(Case + i)->type,
					(Case + i)->price,
					(Case + i)->quantity,
					(Case + i)->id);
				printf("How much Case you wont sell:\n");
				scanf(" %d", &sellQuantity);
				if (sellQuantity > (Case + i)->quantity) {
					printf("That purches for this Case is not possible\n");
					free(Case);
					return -1;
				}
				else {
					(Case + i)->quantity = (Case + i)->quantity - sellQuantity;
					printf("New quantity is %d\n", (Case + i)->quantity);
					break;
				}
			}
		}
		if (flag == 0) {
			free(Case);
			return -1;
		}
		//entering new value of quantity in the file 
		remove("Case.bin");
		file = fopen("Case.bin", "wb");
		if (file == NULL) {
			perror("Opening dat:");
			exit(EXIT_FAILURE);
		}
		else {
			rewind(file);
			fwrite(counter, sizeof(int), 1, file);
			fseek(file, 4, SEEK_SET);
			for (int i = 0; i < (*counter); i++) {
				fwrite((Case + i), sizeof(PRODUCT_TYPE3), 1, file);
			}
		}
		fclose(file);
		free(Case);
		return 1;
	}
	else {
		PRODUCT_TYPE3* Cooler = NULL;
		Cooler = GetingProductType3(Cooler, counter, "Cooler.bin");
		if (Cooler == NULL) {
			printf("Fail.");
			exit(EXIT_FAILURE);
		}
		for (int i = 0; i < *(counter); i++) {
			if ((strcmp((Cooler + i)->name, nameOfSearchItem) == 0) && (Cooler + i)->id == idOfSearchItem) {
				flag = 1;
				printf("\nItem is found.\n");
				printf("\nName of Cooler:%s\nType of Cooler:%s\nPrice of Cooler:%f\nQuantity:%d\nId of product:%d\n",
					(Cooler + i)->name,
					(Cooler + i)->type,
					(Cooler + i)->price,
					(Cooler + i)->quantity,
					(Cooler + i)->id);
				printf("How much Cooler you want sell:\n");
				scanf(" %d", &sellQuantity);
				if (sellQuantity > (Cooler + i)->quantity) {
					printf("That purchase for this Case is not possible\n");
					free(Cooler);
					return -1;
				}
				else {
					(Cooler + i)->quantity = (Cooler + i)->quantity - sellQuantity;
					printf("New quantity is %d\n", (Cooler + i)->quantity);
					break;
				}
			}
		}
		if (flag == 0) {
			free(Cooler);
			return -1;
		}
		//entering new value of quantity in the file 
		remove("Cooler.bin");
		file = fopen("Cooler.bin", "wb");
		if (file == NULL) {
			perror("Opening dat:");
			exit(EXIT_FAILURE);
		}
		else {
			rewind(file);
			fwrite(counter, sizeof(int), 1, file);
			fseek(file, 4, SEEK_SET);
			for (int i = 0; i < (*counter); i++) {
				fwrite((Cooler + i), sizeof(PRODUCT_TYPE3), 1, file);
			}
		}
		fclose(file);
		free(Cooler);
		return 1;
	}
	return -1;
}